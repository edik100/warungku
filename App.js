import React, {Component} from 'react';

import { View, Text, Button, TextInput, StyleSheet, Image } from 'react-native';
import { createAppContainer, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Router from './src/config/router';

import 'react-native-gesture-handler';


class App extends Component {
    render(){
        return(
        <Router/>
        
        );
    }
}

export default App;


