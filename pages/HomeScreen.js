import React from 'react';
import { View, Text, Button } from 'react-native';
import {
    createStackNavigator
} from 'react-navigation-stack';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Profile... again"
          onPress={() => this.props.navigation.push('Profile')}
        />
      </View>
    );
  }
}