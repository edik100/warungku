import React, {Component} from 'react';
import {} from 'react-native';
import {} from 'react-navigation';
import HomeScreen from 'HomeScreen';
import ProfileScreen from 'ProfileScreen';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createStackNavigator } from 'react-navigation-stack';

export class Login extends Component {
    render(){
        return(
            <View>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Home')}>
                    <Text>Sign In</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default class App extends Component {
    render(){
        return(
            <AppStackNavigator/>
        );
    }
}
const AppStackNavigator = createStackNavigator ({
    Home: HomeScreen,
    Profile: ProfileScreen
});