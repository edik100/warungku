import { createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Home, Foods, Kategori, Table, AddTable, AddFoods, AddCategory } from '../../containers/pages';

const Router = createStackNavigator(
    { 
        Home:{
            screen:Home,
        },
        Foods: {
            screen:Foods,
        },
        Kategori: {
            screen: Kategori,
        },
        Table: {
            screen: Table,
        },
        AddTable: {
            screen: AddTable,
        },
        AddFoods: {
            screen: AddFoods,
        },
        AddCategory: {
            screen: AddCategory
        },
        
    },
    {
        headerMode: 'none',
        initialRouteName: 'Home'
    }
);
export default createAppContainer(Router);
