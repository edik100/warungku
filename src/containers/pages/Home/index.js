import React ,{Component} from 'react';
import { View, Text,  TextInput, StyleSheet, Image, Button } from 'react-native';
import 'react-native-gesture-handler';

class Home extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View >
                    <Image style={styles.imglogo} source={require('../../../assets/img/logorozaka.png')}></Image>
                    {/* <Text style={styles.textcon}>ROZAKA</Text> */}
                </View>
                <View>
                    <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Email"
                    placeholderTextColor="#ffffff"
                    selectionColor="#fff"
                    keyboardType="email-address"
                    onSubmitEditing={() => this.password.focus()}
                    />
                    <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#ffffff"
                    ref={(input) => this.password = input}
                    />
                </View>
                <View>
                    <Text style={styles.BoxText}
                    onPress={() => this.props.navigation.navigate('Foods')}>SIGN IN</Text> 
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'grey'
    },
    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    BoxText: {
        width: 300,
        height: 50,
        backgroundColor: 'black',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 20,
        color: '#ffffff',
        marginVertical: 10,
        paddingVertical: 9,
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    textcon: {
        fontSize: 70,
        fontWeight: 'bold',
        color: '#fff',
        marginBottom: 12

    },
    imglogo: {
        width: 300,
        height: 100,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        marginBottom: 15
    }

});

export default Home;