import { View, Text, TextInput, StyleSheet, Modal, TouchableHighlight, Alert } from 'react-native';
import React, {Component} from 'react';
import { Button, Header,Overlay, SearchBar, ListItem, SocialIcon, Icon, Input } from 'react-native-elements';


class  AddTable extends Component {
      state = {
            overlayVisible: false,
        };

        setOverlayVisible(visible){
            this.setState({overlayVisible: visible})
        }
       
    render(){
        return ( 
            <View>
                    <Overlay
                    isVisible={this.state.isVisible}
                    onBackdropPress={() => this.setState({ isVisible: false })}
                    >
                        <View>
                            <Header
                            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
                            centerComponent={{ text: ' ADD TABLES', style: { color: '#fff' } }}
                            />
                            
                        </View>
                        <View style={{flex:1,marginTop:20}}>
                            <Text>Name Table</Text>
                            <Input
                            placeholder='Insert Name Table'                          
                            />
                            <Text>Sit Capacity</Text>
                            <Input
                            placeholder='Sit Capacity'
                            />
                            <Text>Position </Text>
                            <Input
                            placeholder='Position Table'
                            />
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{flex:1, marginRight:6}}>
                            <Button
                            title='BACK'
                            onPress={() => this.props.navigation.goBack()} 
                            />
                            </View>
                            <View style={{flex:1, marginLeft:6}}>
                            <Button
                            title='SAVE'
                            onPress={() => this.props.navigation.navigate('Table')} 
                            />
                            </View>

                        </View>
                    </Overlay>        
            </View>
        );
    }
}
const styles = StyleSheet.create ({
    container:{
        
    }
})


export default AddTable;