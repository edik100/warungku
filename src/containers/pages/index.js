import Home from './Home';
import Foods from './Foods';
import Kategori from './Kategori';
import Table from './Table';
import AddTable from './AddTable';
import AddFoods from './AddFoods';
import AddCategory from './AddCategory'
export{Home, Foods, Kategori, Table, AddTable, AddFoods, AddCategory}