import React  from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Button, Header, SearchBar, ListItem, SocialIcon, Icon, Overlay, Input } from 'react-native-elements';
import 'react-native-gesture-handler';

class Table extends React.Component {
  state = {
      isVisible: false,
    };

    setisVisible(visible) {
      this.setState({isVisible: visible});
    }
  render() {
    return (
      <View>
        <Overlay isVisible={this.state.isVisible} 
                    onBackdropPress={() => this.setisVisible( false )}>
                    <View>
                            <Header
                            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
                            centerComponent={{ text: ' ADD TABLES', style: { color: '#fff' } }}
                            />
                            
                        </View>
                        <View style={{flex:1,marginTop:20}}>
                            <Text>Name Table</Text>
                            <Input
                            placeholder='Insert Name Table'                          
                            />
                            <Text>Sit Capacity</Text>
                            <Input
                            placeholder='Sit Capacity'
                            />
                            <Text>Position </Text>
                            <Input
                            placeholder='Position Table'
                            />
                        </View>
                        
                            <View style={{ margin:6}}>
                            <Button
                            title='SAVE'
                            onPress={() => this.props.navigation.navigate('Table')} 
                            />
                            </View>
                  </Overlay>
          <View>
              <Header 
              containerStyle={{backgroundColor: 'black',justifyContent: 'space-around', margin:0}}
                centerComponent={{ text: 'TABLE', style: { color: '#fff', fontSize:20, marginBottom:15 } }}
                rightComponent={{ icon: 'playlist-add', color: '#fff', marginBottom:20, 
                onPress: () => {this.setisVisible(true)}
              }}
                />
          </View>
              <View>
              <ListItem
                title='MEJA 1'
                bottomDivider
                subtitle={
                        <View style={styles.subtitleView}>
                        <Text style={styles.ratingText}>Slot Maximal : 5 Orang</Text>
                        </View>
                        }
                 leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
                />
                <ListItem
                title='MEJA 2'
                bottomDivider
                subtitle={
                        <View style={styles.subtitleView}>
                        
                        <Text style={styles.ratingText}>Slot Maximal : 6 Orang</Text>
                        </View>
                        }
                 leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
                />
                <ListItem
                title = 'MEJA 3'
                bottomDivider
                subtitle={
                        <View style={styles.subtitleView}>
                       
                        <Text style={styles.ratingText}>Slot Maximal : 6 Orang</Text>
                        </View>
                        }
                 leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
                />
          </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    subtitleView: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingTop: 5
    },
    ratingImage: {
        height: 19.21,
        width: 100
    },
    ratingText: {
        paddingLeft: 10,
        color: 'grey'
    }
})

export default Table;