import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import { Button, Header, SearchBar, ListItem, SocialIcon, Icon, Overlay, Input } from 'react-native-elements';
import {DrawerItems, createDrawerNavigator} from 'react-navigation-drawer';
import { createAppContainer, SafeAreaView, DrawerNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import 'react-native-gesture-handler';
import Table from '../Table';
import Kategori from '../Kategori';


class Foods extends React.Component {
  state = {
      isVisible: false,
    };

    setisVisible(visible) {
      this.setState({isVisible: visible});
    }

  render() {
    return (
      <View>
         <Overlay isVisible={this.state.isVisible} 
                    onBackdropPress={() => this.setisVisible(false)}>
                    <View>
                            <Header
                            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
                            centerComponent={{ text: 'ADD FOODS', style: { color: '#fff' } }}
                            />
                            
                        </View>
                        <View style={{flex:1,marginTop:20}}>
                            <Text>Name Foods</Text>
                            <Input
                            placeholder='Insert Name Foods'                          
                            />
                            <Text>Description</Text>
                            <Input
                            placeholder='Description Foods'
                            />
                            
                        </View>
                        
                            <View style={{margin:6}}>
                            <Button
                            title='SAVE'
                            onPress={() => this.props.navigation.navigate('Foods')} 
                            />
                            </View>
                  </Overlay>
        <View>
          <Header
            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
            centerComponent={{text: 'FOODS', style: {color: '#fff', fontSize:20, marginBottom:15}}}
            rightComponent={{icon: 'playlist-add',color: '#fff', 
            onPress: () => {this.setisVisible(true)}
              // onPress: () => this.props.navigation.navigate('AddFoods'), marginBottom: 20,
            }}
          />
        </View>
            {/* Button:
              onPress: () => {
                this.setisVisible(true)
              }
            Overlay: */}
           
                {/* onBackdropPress={() => this.setisVisible(false) */}
            <ScrollView>
        <View style={styles.foodcontainer}>
          
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
            <View style={styles.foodbox}>
              <Image source={require('../../../assets/img/logorozaka.png')} style={styles.Image} />
                  <Text style={styles.text}>Rp. 10.000</Text>
            </View>
        </View>
          </ScrollView>
          
      </View>
    );
  }
}

const AppDrawNavigator = createDrawerNavigator ({
  TABLE : Table,
  FOODS : Foods,
  CATEGORY : Kategori,
  
});

const styles = StyleSheet.create({
    subtitleView: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingTop: 5
    },
    Image: {
        height: 70,
        width: 140,
        resizeMode:'cover'
    },
    text: {
      paddingTop:10,
        paddingLeft: 10,
        color: 'grey',
    },
    foodcontainer: {
      flexWrap:'wrap',
      flexDirection:'row'
    },
    
    foodbox:{
      borderRadius:25,
      // backgroundColor:'red',
      margin : 3,
      height:150,
      alignItems:'center',
      justifyContent:'center'
    }
})
export default createAppContainer(AppDrawNavigator);