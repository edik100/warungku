import { View, Text, TextInput, StyleSheet, Modal, TouchableHighlight, Alert } from 'react-native';
import React, {Component} from 'react';
import { Button, Header,Overlay, SearchBar, ListItem, SocialIcon, Icon, Input } from 'react-native-elements';


class  Kategori extends Component {
      state = {
      isVisible: false,
    };

    setisVisible(visible) {
      this.setState({isVisible: visible});
    }
  render() {
    return (
      <View>
          <Overlay isVisible={this.state.isVisible} 
                    onBackdropPress={() => this.setisVisible(false)}>
                    <View>
                            <Header
                            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
                            centerComponent={{ text: 'ADD CATEGORY', style: { color: '#fff' } }}
                            />
                            
                        </View>
                        <View style={{flex:1,marginTop:20}}>
                            <Text>Name Category</Text>
                            <Input
                            placeholder='Insert Name Category'                          
                            />
                            <Text>Description</Text>
                            <Input
                            placeholder='Description Category'
                            />
                            
                        </View>
                            <View style={{ margin:6}}>
                            <Button
                            title='SAVE'
                            onPress={() => this.props.navigation.navigate('Kategori')} 
                            />
                            </View>

            </Overlay>
        <View>
          <Header
            containerStyle={{backgroundColor: 'black',justifyContent: 'space-around',}}
            centerComponent={{text: 'CATEGORY', style: {color: '#fff', fontSize:20, marginBottom:15}}}
            rightComponent={{icon: 'playlist-add',color: '#fff', 
              onPress: () => {this.setisVisible(true)}, marginBottom: 20,
            }}
          />
        </View>
        <ListItem
            title='MAKANAN'
            bottomDivider
            chevron
            rightIcon={{}}
            onPress={() => this.props.navigation.navigate( 'AddFoods')}
            leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
            />
        <ListItem
          title='MINUMAN'
          bottomDivider
          chevron
           leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
          />
          <ListItem
          title='DESSERT'
          bottomDivider
          chevron
           leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
          />
        <ListItem
          title='ADDS ONS'
          bottomDivider
          chevron
           leftAvatar={{ source: require('../../../assets/img/logorozaka.png') }}
          />
      </View>
    );
  }
}


const styles = StyleSheet.create({
    subtitleView: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingTop: 5
    },
    ratingImage: {
        height: 19.21,
        width: 100
    },
    ratingText: {
        paddingLeft: 10,
        color: 'grey'
    }
})
export default Kategori;